import { writeFileSync, readFileSync, existsSync, copyFileSync } from 'fs';
import { resolve, join } from 'path';
import { execSync } from 'child_process';
import MarkdownIt from 'markdown-it';
import { removeFolder, extractData, parseMdImage, relativeCopyFile } from '@pub-tools/mdlib-ts';
import { createNewDirectory, createSubDirectories, writePreProcessedDestFile, preProcessKrokiMdContent } from '@dgwnu/md-pre-kroki';
import { readMdBookConfigFile } from '@pub-tools/mdbook';

/**
 * Markdown Item Parsing functionality
 * Based on MarkdowIt structures
 */
// Constants as stable config for Markdown parsing options
var DEBUG_OUTPUT_PATH = 'debug-output';
var HTML_IMG_TAG = { start: '<img ', end: ' />' };
var HTML_HEADER_LEVEL_1_TAG = 'h1';
var LIST_ITEM_LEVEL_SPACE = ' ';
var FOOTNOTE_MARKUP = { start: '[^', end: ']' };
var INLINE_FOOTNOTE_MARKUP = { start: '^[', end: ']' };
var MD_LIST_MARKUP = '-';
var MD_HEADER_MARKUP = '#';
/**
 * Item index reference class
 */
var ItemIndex = /** @class */ (function () {
    function ItemIndex(_currentIndex) {
        this._currentIndex = _currentIndex;
    }
    Object.defineProperty(ItemIndex.prototype, "currentIndex", {
        get: function () { return this._currentIndex; },
        set: function (value) {
            this._currentIndex = value;
        },
        enumerable: false,
        configurable: true
    });
    return ItemIndex;
}());
/**
 * Markdown Chapter Content
 */
var MdChapter = /** @class */ (function () {
    function MdChapter(_chapterItem) {
        this._chapterItem = _chapterItem;
        this._items = [];
        this._footNotes = [];
    }
    Object.defineProperty(MdChapter.prototype, "chapterItem", {
        get: function () {
            return this._chapterItem;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdChapter.prototype, "chapterName", {
        /**
         * chapter name
         */
        get: function () {
            return createChapterName(this);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdChapter.prototype, "chapterMdName", {
        /**
         * chapter name with markup
         */
        get: function () {
            return createChapterMdName(this);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdChapter.prototype, "mdFileName", {
        /**
         * Chapter Markdown filename
         */
        get: function () {
            return createChapterMdFileName(this);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdChapter.prototype, "mdContent", {
        /**
         * Chapter Markdown content
         */
        get: function () {
            return createMdChapterContent(this);
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Add Chapter Markdown content item
     * @param item
     */
    MdChapter.prototype.addItem = function (item) {
        this._items.push(item);
    };
    Object.defineProperty(MdChapter.prototype, "items", {
        /**
         * Chapter Markdown content items
         */
        get: function () {
            return this._items;
        },
        enumerable: false,
        configurable: true
    });
    MdChapter.prototype.addFootNote = function (footNote) {
        this._footNotes.push(footNote);
    };
    Object.defineProperty(MdChapter.prototype, "footNotes", {
        get: function () {
            return this._footNotes;
        },
        enumerable: false,
        configurable: true
    });
    return MdChapter;
}());
function createChapterName(mdChapter) {
    return mdChapter.chapterItem.lines[0].content.trim();
}
function createChapterMdFileName(mdChapter) {
    return mdChapter.chapterName.split(' ').join('_').toLowerCase() + '.md';
}
function createChapterMdName(mdChapter) {
    return createMdHeaderContent(mdChapter.chapterItem);
}
function createMdChapterContent(mdChapter) {
    // start with markdown chapter header
    var chapterContent = mdChapter.chapterMdName + '\n\n';
    // add markdown item content
    for (var _i = 0, _a = mdChapter.items; _i < _a.length; _i++) {
        var item = _a[_i];
        chapterContent += createMdItemContent(item);
    }
    // parse chapter footnotes in: markdown-it-footnote "Inline footnote" style (^[footnote text])
    // see https://github.com/markdown-it/markdown-it-footnote
    for (var _b = 0, _c = mdChapter.footNotes; _b < _c.length; _b++) {
        var footnote = _c[_b];
        var splittedContent = chapterContent.split(FOOTNOTE_MARKUP.start + footnote.id + FOOTNOTE_MARKUP.end);
        chapterContent = splittedContent[0] + INLINE_FOOTNOTE_MARKUP.start + footnote.content + INLINE_FOOTNOTE_MARKUP.end + splittedContent[1];
    }
    return chapterContent;
}
/**
 * Map a Markdown content string into typed Markdown Content Chapters Content Items
 * @param mdContentStr Markdown formated string (required)
 * @param defaultFirstChapter Chapter to be set when there is no starting Header Level 1 (optional)
 * @returns Array Chapters Content Items
 */
function mapMdChapters(mdContentStr, defaultFirstChapter) {
    if (defaultFirstChapter === void 0) { defaultFirstChapter = 'Inleiding'; }
    // remove previous created folders
    removeFolder(DEBUG_OUTPUT_PATH);
    // read, chunk and parse markdown content into chapters and footnotes
    var mappedMdContent = mapMdItems(mdContentStr);
    writeDebugVarData('mappedMdContent', mappedMdContent);
    var mdChapters = [];
    if (mappedMdContent.mdContentItems.length > 0) {
        var itemIndex = 0;
        var chapterIndex = -1;
        if (mappedMdContent.mdContentItems[itemIndex].tag != HTML_HEADER_LEVEL_1_TAG) {
            // If first chapter has no Header (Level 1),
            // then add default first chapter Header
            chapterIndex = mdChapters.push(new MdChapter({
                type: 'heading',
                tag: HTML_HEADER_LEVEL_1_TAG,
                level: 0,
                markup: MD_HEADER_MARKUP,
                lines: [{
                        type: 'inline',
                        tag: '',
                        level: 1,
                        markup: '',
                        content: defaultFirstChapter
                    }]
            })) - 1;
        }
        while (itemIndex < mappedMdContent.mdContentItems.length) {
            var mdContentItem = mappedMdContent.mdContentItems[itemIndex];
            if (mdContentItem.tag == HTML_HEADER_LEVEL_1_TAG && mdContentItem.lines[0].content != '') {
                // Header Level 1 that is not empty starts new Chapter
                chapterIndex = mdChapters.push(new MdChapter(mdContentItem)) - 1;
            }
            else if (mdContentItem.tag != HTML_HEADER_LEVEL_1_TAG) {
                // Process Next Chapter Items with related Footnotes
                addChapterItem(mdChapters[chapterIndex], mdContentItem, mappedMdContent.footNotes);
            }
            itemIndex++;
        }
    }
    writeDebugVarData('mdChapters', mdChapters);
    return mdChapters;
}
function addChapterItem(chapter, mdContentItem, footNotes) {
    chapter.addItem(mdContentItem);
    for (var _i = 0, _a = mdContentItem.lines; _i < _a.length; _i++) {
        var line = _a[_i];
        if (line.type == 'inline') {
            // prepare specialized inline content
            var lineContent = line.content;
            var _loop_1 = function () {
                // line(part) contains footnote to append
                var footNoteId = extractData(lineContent, FOOTNOTE_MARKUP.start, FOOTNOTE_MARKUP.end);
                var footNoteMarkup = "" + FOOTNOTE_MARKUP.start + footNoteId + FOOTNOTE_MARKUP.end;
                var footNote = footNotes.find(function (footNote) { return footNote.startsWith(footNoteMarkup); });
                if (footNote) {
                    chapter.addFootNote({
                        id: footNoteId,
                        content: footNote.split(footNoteMarkup + ': ')[1]
                    });
                }
                else {
                    console.warn("Footnote = " + footNoteMarkup + " not found!");
                }
                // split line part that is not yet processed
                lineContent = lineContent.split(footNoteMarkup)[1];
            };
            while (lineContent.split(FOOTNOTE_MARKUP.start).length > 1) {
                _loop_1();
            }
        }
    }
}
/**
 * Map Markdown content strings into typed Markdown Content Items
 * @param mdContentStr Markdown formated string
 * @returns (Array with mapped Markdown Content Items, Array with mapped Footnotes)
 */
function mapMdItems(mdContentStr) {
    // init variables and parse Markdown content data
    var mdContentItems = [];
    var footNotes = [];
    var markdownIt = new MarkdownIt();
    var markdownItLines = markdownIt.parse(mdContentStr, {});
    var itemIndex = new ItemIndex(0);
    // map parsed Markdown content data
    while (itemIndex.currentIndex < markdownItLines.length) {
        var itemType = markdownItLines[itemIndex.currentIndex].type.split('_open')[0];
        //console.log(`mapMdItems ==> itemType[${itemIndex.currentIndex}] = ${itemType}`);
        mdContentItems.push(mapMdContentItem(markdownItLines, itemIndex, itemType, footNotes));
        itemIndex.currentIndex++;
    }
    return { mdContentItems: mdContentItems, footNotes: footNotes };
}
/**
 * Map a MarkedIt-parsed object to a typed Markdown Content Item
 * @param markdownItLines Array with parsed MarkedownIt Line-objects (required)
 * @param indexRef Index reference to MarkedownIt Line-object (required)
 * @param itemType Type of Markdown Content Item to map (required)
 * @param footNotes Reference to mapped footnote content (required)
 * @returns Object with typed Markdown Content Item and Lines
 */
function mapMdContentItem(markdownItLines, indexRef, itemType, footNotes) {
    // create initial object item-type and parsed MarkedIt object values
    var contentItem = {
        type: itemType,
        tag: markdownItLines[indexRef.currentIndex].tag,
        level: markdownItLines[indexRef.currentIndex].level,
        markup: markdownItLines[indexRef.currentIndex].markup,
        lines: []
    };
    // create object lines within item-type
    indexRef.currentIndex++;
    while (!lastMarkdownItLineItem(markdownItLines[indexRef.currentIndex], contentItem)) {
        var prepContent = prepareMarkdownItLineContent(markdownItLines[indexRef.currentIndex].content);
        if (markdownItLines[indexRef.currentIndex].content.startsWith(FOOTNOTE_MARKUP.start)) {
            footNotes.push(prepContent);
        }
        else {
            contentItem.lines.push({
                type: markdownItLines[indexRef.currentIndex].type,
                tag: markdownItLines[indexRef.currentIndex].tag,
                level: markdownItLines[indexRef.currentIndex].level,
                markup: markdownItLines[indexRef.currentIndex].markup,
                content: prepContent
            });
        }
        indexRef.currentIndex++;
    }
    return contentItem;
}
/**
 * Determine end of content item lines (on the same level)
 * @param markdownItLine current MarkdownIt line to map
 * @param contentItem current content item to map
 * @returns weither current MarkdownIt line is the end of current content item that is mapped
 */
function lastMarkdownItLineItem(markdownItLine, contentItem) {
    return (markdownItLine.type == contentItem.type + '_close' && markdownItLine.level == contentItem.level);
}
function prepareMarkdownItLineContent(contentInput) {
    var contentOutput = contentInput;
    if (contentInput.split(HTML_IMG_TAG.start).length > 1) {
        // convert HTML image to Markdown variant
        var imageData = extractData(contentInput, HTML_IMG_TAG.start, HTML_IMG_TAG.end);
        var imageRef = extractData(imageData, 'src="', '"');
        var imageId = 'imageId' + String(Math.round((Math.random() * 10000)));
        contentOutput = parseMdImage(imageId, imageRef);
    }
    return contentOutput;
}
/**
 * create Markdown content from Item Object
 * @param mdContentItem Item Object for Markdown Content
 * @returns string with Markdown content
 */
function createMdItemContent(item) {
    var mdContent = '';
    switch (item.type) {
        case 'heading': {
            mdContent = createMdHeaderContent(item);
            break;
        }
        case 'table': {
            mdContent = createMdTableContent(item);
            break;
        }
        case 'bullet_list': {
            mdContent = createMdListContent(item);
            break;
        }
        case 'ordered_list': {
            mdContent = createMdListContent(item);
            break;
        }
        case 'paragraph': {
            mdContent = createMdParagraphContent(item);
            break;
        }
    }
    return mdContent;
}
function createMdHeaderContent(item) {
    var headerLevel = Number(item.tag.split('h')[1]);
    var mdMarkup = MD_HEADER_MARKUP.repeat(headerLevel) + ' ';
    return createMdContent(item, mdMarkup);
}
function createMdListContent(item) {
    var mdContent = '';
    var seqNr = 0;
    for (var _i = 0, _a = item.lines; _i < _a.length; _i++) {
        var line = _a[_i];
        switch (line.type) {
            case 'list_item_open': {
                // add list item level spaces and list item ident
                if (line.level > 1) {
                    mdContent += LIST_ITEM_LEVEL_SPACE.repeat(line.level - 1);
                }
                if (item.type == 'ordered_list') {
                    // set specific content for ordered list
                    seqNr++;
                    mdContent += seqNr.toString();
                }
                mdContent += MD_LIST_MARKUP;
                break;
            }
            case 'inline': {
                mdContent += ' ' + line.content + '\n\n';
            }
        }
    }
    return mdContent;
}
function createMdTableContent(item) {
    var mdContent = '';
    var rowContent = '';
    var headContent = '';
    for (var _i = 0, _a = item.lines; _i < _a.length; _i++) {
        var line = _a[_i];
        switch (line.type) {
            case 'th_open': {
                if (!rowContent.trim().endsWith('|')) {
                    rowContent += '| ';
                }
                if (!headContent.trim().endsWith('|')) {
                    headContent += '|';
                }
                headContent += '-';
                break;
            }
            case 'th_close': {
                rowContent += ' | ';
                headContent += '-|';
                break;
            }
            case 'tr_close': {
                mdContent += rowContent.trim() + '\n';
                rowContent = '';
                break;
            }
            case 'thead_close': {
                mdContent += headContent.trim() + '\n';
                headContent = '';
                break;
            }
            case 'inline': {
                rowContent += line.content;
                break;
            }
            case 'td_open': {
                if (!rowContent.trim().endsWith('|')) {
                    rowContent += '| ';
                }
                break;
            }
            case 'td_close': {
                rowContent += ' | ';
                break;
            }
        }
    }
    mdContent += '\n' + rowContent;
    return mdContent;
}
function createMdParagraphContent(item) {
    return createMdContent(item);
}
function createMdContent(item, MdMarkup) {
    if (MdMarkup === void 0) { MdMarkup = ''; }
    var mdContent = '';
    for (var _i = 0, _a = item.lines; _i < _a.length; _i++) {
        var line = _a[_i];
        mdContent += line.content + '\n';
    }
    return "" + MdMarkup + mdContent + '\n';
}
/**
 * Synchronized execution of PanDoc CLI
 * @param parmsFrom PanDoc -f/--from parms (required)
 * @param parmsTo PanDoc -t/--to parms
 * @param inputFilePath path and name input file
 * @param outputPath path output file's processing (including attached images etc.)
 * @param outputFile main output file name (a Markdown file or PDF, etc.)
 * @returns terminal output execution
 */
function panDocExecSync(parmsFrom, parmsTo, inputFilePath, outputPath, outputFile) {
    // PanDoc CLI execution
    var absInputFilePath = resolve(inputFilePath);
    createNewDirectory(outputPath);
    var panDocArgs = "-f " + parmsFrom.join(' ') + " -t " + parmsTo.join(' ') + " \"" + absInputFilePath + "\" -o \"" + outputFile + "\"";
    console.log("panDocArgs = " + panDocArgs);
    return execSync("pandoc " + panDocArgs, { cwd: outputPath }).toString();
}
function writeDebugVarData(varName, varData) {
    createNewDirectory(DEBUG_OUTPUT_PATH);
    writeFileSync(resolve(DEBUG_OUTPUT_PATH, varName + ".json"), JSON.stringify(varData, undefined, '  '));
}

/**
 * Doc2Md Utils
 */
/**
 * Constant Values
 */
var PANDOC_MD_OUTPUT_FILE = 'converted-by-pandoc.md';
var PANDOC_DOCX_PARMS_FROM = ['docx'];
var PANDOC_MD_PARMS_TO = [
    'markdown_strict+footnotes+pipe_tables',
    '--wrap=none', '--tab-stop=2',
    '--extract-media=.'
];
var SUMMARY_MD_FILE = 'SUMMARY.md';
var FIRST_CHAPTER_FILE = 'README.md';
var CONTRIBUTING_FILE = 'CONTRIBUTING.md';
var CHANGELOG_FILE = 'CHANGELOG.md';
var MDBOOK_CONFIG = 'mdbook-config.json';
/**
 * Converts a google or word docx file to multiple markdown-files (for each section) and the embedded media asset files
 * @param relDocxFilePath relative path and file name docx-document (required)
 * @param mdFilesPath path to directory to write converted Markdown and Media-assets files (required)
 * @param panDocOutputPath temporary PanDoc conversion output path (optional, default = pandoc-output)
 */
function convertDocx2MdBook(docxFilePath, mdFilesPath, panDocOutputPath) {
    if (panDocOutputPath === void 0) { panDocOutputPath = 'pandoc-output'; }
    // remove previous created folders
    removeFolder(mdFilesPath);
    removeFolder(panDocOutputPath);
    // create markdown file and extract media from docx file
    docx2MdFile(docxFilePath, panDocOutputPath);
    // split-up converted markdown file into chapters and do some pre-processing (kroki.io, images, footnotes, ...)
    var absPanDocOutputFilePath = resolve(panDocOutputPath, PANDOC_MD_OUTPUT_FILE);
    var mdChapters = mapMdChapters(readFileSync(absPanDocOutputFilePath, 'utf-8'));
    var srcDir = resolve(panDocOutputPath);
    var destDir = resolve(mdFilesPath);
    // use create sub directories for one file to create a sub directories for all output markdown files
    createSubDirectories(resolve('.'), join(mdFilesPath, 'stubfile.md'));
    var firstChapter = true;
    var summaryLines = [];
    for (var _i = 0, mdChapters_1 = mdChapters; _i < mdChapters_1.length; _i++) {
        var mdChapter = mdChapters_1[_i];
        var chapterContent = mdChapter.mdContent;
        var chapterFile = mdChapter.mdFileName;
        if (firstChapter) {
            chapterFile = FIRST_CHAPTER_FILE;
            firstChapter = false;
        }
        console.log("srcDir = " + srcDir + " destDir = " + destDir + " chapterFile = " + chapterFile);
        writePreProcessedDestFile(srcDir, destDir, join(srcDir, chapterFile), preProcessKrokiMdContent(chapterContent));
        summaryLines.push(createSummaryLine(mdChapter.chapterName, chapterFile));
    }
    // write basic files with default / custom publication content and required configuration
    writeBasicFile(destDir, '# Bijdragen\n\n - Gegenereerd door Docx2MdBook CLI\n', CONTRIBUTING_FILE);
    writeBasicFile(destDir, '# Wijzigingen\n\n - Gegenereerd door Docx2MdBook CLI\n', CHANGELOG_FILE);
    writeSummaryFile(summaryLines, destDir);
    writeMdBookConfig(destDir);
}
/**
 * Convert a docx file to a  Markdown file (and extract media)
 * @param docxFilePath path / file input docx file
 * @param panDocOutputPath path markdown file (media will be extracted to path)
 */
function docx2MdFile(docxFilePath, panDocOutputPath) {
    console.log(panDocExecSync(PANDOC_DOCX_PARMS_FROM, PANDOC_MD_PARMS_TO, docxFilePath, panDocOutputPath, PANDOC_MD_OUTPUT_FILE));
}
/**
 * Write a customized basic file found in the repository root or default content if not supplied
 * @param destDir directory path where to write the basic file content (required)
 * @param defaultContent default content in case there is no basic file supllied in root (required)
 * @param fileName basic file name (required)
 */
function writeBasicFile(destDir, defaultContent, fileName) {
    var srcFilePath = resolve('.', fileName);
    if (existsSync(srcFilePath)) {
        // customized basic source file found in root
        var customizedContent = readFileSync(srcFilePath, 'utf-8');
        var srcDir = resolve('.');
        writePreProcessedDestFile(srcDir, destDir, srcFilePath, customizedContent);
    }
    else {
        // no customized basic source file available
        writeFileSync(resolve(destDir, fileName), defaultContent, 'utf-8');
    }
}
/**
 * Create a new summary line from id and linked md file path
 * @param summaryId id or name of summary line (required)
 * @param mdFilePath linked file path (required)
 * @returns a GitBook like formated summary line string
 */
function createSummaryLine(chapterName, mdChapterFile) {
    return "* [" + chapterName + "](" + mdChapterFile + ")";
}
/**
 * write summary lines to destination directory
 * @param summaryLines list with summary lines to write
 * @param destDir destination to write SUMMARY.md file
 */
function writeSummaryFile(summaryLines, destDir) {
    var summaryDestFilePath = resolve(destDir, SUMMARY_MD_FILE);
    var summaryContent = [
        '# Converted from docx to MdBook\n\n',
        '## Over dit document\n\n',
        summaryLines[0] + '\n',
        '* [Bijdragen](' + CONTRIBUTING_FILE + ')\n',
        '* [Wijzigingen](' + CHANGELOG_FILE + ')\n\n',
        '## Inhoud\n\n',
        summaryLines.slice(1).join('\n'),
        '\n'
    ].join('');
    writeFileSync(summaryDestFilePath, summaryContent, 'utf-8');
}
/**
 * Write required MdBook Configuration that should be supplied in the root of the repository
 * @param destDir destination directory where to write the MdBook Configuration
 *
 * @todo
 * - Generic copy with auto create directory.... (mdlib-ts ??)
 */
function writeMdBookConfig(destDir) {
    if (existsSync(MDBOOK_CONFIG)) {
        // copy the config to destination
        var mdBookConfigFilePath = resolve(destDir, MDBOOK_CONFIG);
        copyFileSync(MDBOOK_CONFIG, mdBookConfigFilePath);
        // read tge config and copy linked files (logo etc.)
        var mdBookConfig = readMdBookConfigFile(mdBookConfigFilePath);
        if (mdBookConfig.logo) {
            // copy linked logo file
            relativeCopyFile(mdBookConfig.logo, destDir);
        }
    }
    else {
        console.warn("MdBook Configuration file " + MDBOOK_CONFIG + " is missing!");
    }
}

export { convertDocx2MdBook };
