"use strict";
/**
 * Markdown Item Parsing functionality
 * Based on MarkdowIt structures
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.panDocExecSync = exports.createMdContent = exports.createMdParagraphContent = exports.createMdTableContent = exports.createMdListContent = exports.createMdHeaderContent = exports.createMdItemContent = exports.mapMdChapters = void 0;
//import { inspect } from 'util';
const child_process_1 = require("child_process");
const path_1 = require("path");
const fs_1 = require("fs");
const markdown_it_1 = __importDefault(require("markdown-it"));
const mdlib_ts_1 = require("@pub-tools/mdlib-ts");
const md_pre_kroki_1 = require("@dgwnu/md-pre-kroki");
// Constants as stable config for Markdown parsing options
const DEBUG_OUTPUT_PATH = 'debug-output';
const HTML_IMG_TAG = { start: '<img ', end: ' />' };
const HTML_HEADER_LEVEL_1_TAG = 'h1';
const LIST_ITEM_LEVEL_SPACE = ' ';
const FOOTNOTE_MARKUP = { start: '[^', end: ']' };
const INLINE_FOOTNOTE_MARKUP = { start: '^[', end: ']' };
const MD_LIST_MARKUP = '-';
const MD_HEADER_MARKUP = '#';
/**
 * Item index reference class
 */
class ItemIndex {
    constructor(_currentIndex) {
        this._currentIndex = _currentIndex;
    }
    set currentIndex(value) {
        this._currentIndex = value;
    }
    get currentIndex() { return this._currentIndex; }
}
/**
 * Markdown Chapter Content
 */
class MdChapter {
    constructor(_chapterItem) {
        this._chapterItem = _chapterItem;
        this._items = [];
        this._footNotes = [];
    }
    get chapterItem() {
        return this._chapterItem;
    }
    /**
     * chapter name
     */
    get chapterName() {
        return createChapterName(this);
    }
    /**
     * chapter name with markup
     */
    get chapterMdName() {
        return createChapterMdName(this);
    }
    /**
     * Chapter Markdown filename
     */
    get mdFileName() {
        return createChapterMdFileName(this);
    }
    /**
     * Chapter Markdown content
     */
    get mdContent() {
        return createMdChapterContent(this);
    }
    /**
     * Add Chapter Markdown content item
     * @param item
     */
    addItem(item) {
        this._items.push(item);
    }
    /**
     * Chapter Markdown content items
     */
    get items() {
        return this._items;
    }
    addFootNote(footNote) {
        this._footNotes.push(footNote);
    }
    get footNotes() {
        return this._footNotes;
    }
}
function createChapterName(mdChapter) {
    return mdChapter.chapterItem.lines[0].content.trim();
}
function createChapterMdFileName(mdChapter) {
    return mdChapter.chapterName.split(' ').join('_').toLowerCase() + '.md';
}
function createChapterMdName(mdChapter) {
    return createMdHeaderContent(mdChapter.chapterItem);
}
function createMdChapterContent(mdChapter) {
    // start with markdown chapter header
    let chapterContent = mdChapter.chapterMdName + '\n\n';
    // add markdown item content
    for (const item of mdChapter.items) {
        chapterContent += createMdItemContent(item);
    }
    // parse chapter footnotes in: markdown-it-footnote "Inline footnote" style (^[footnote text])
    // see https://github.com/markdown-it/markdown-it-footnote
    for (const footnote of mdChapter.footNotes) {
        const splittedContent = chapterContent.split(FOOTNOTE_MARKUP.start + footnote.id + FOOTNOTE_MARKUP.end);
        chapterContent = splittedContent[0] + INLINE_FOOTNOTE_MARKUP.start + footnote.content + INLINE_FOOTNOTE_MARKUP.end + splittedContent[1];
    }
    return chapterContent;
}
/**
 * Map a Markdown content string into typed Markdown Content Chapters Content Items
 * @param mdContentStr Markdown formated string (required)
 * @param defaultFirstChapter Chapter to be set when there is no starting Header Level 1 (optional)
 * @returns Array Chapters Content Items
 */
function mapMdChapters(mdContentStr, defaultFirstChapter = 'Inleiding') {
    // remove previous created folders
    mdlib_ts_1.removeFolder(DEBUG_OUTPUT_PATH);
    // read, chunk and parse markdown content into chapters and footnotes
    const mappedMdContent = mapMdItems(mdContentStr);
    writeDebugVarData('mappedMdContent', mappedMdContent);
    let mdChapters = [];
    if (mappedMdContent.mdContentItems.length > 0) {
        let itemIndex = 0;
        let chapterIndex = -1;
        if (mappedMdContent.mdContentItems[itemIndex].tag != HTML_HEADER_LEVEL_1_TAG) {
            // If first chapter has no Header (Level 1),
            // then add default first chapter Header
            chapterIndex = mdChapters.push(new MdChapter({
                type: 'heading',
                tag: HTML_HEADER_LEVEL_1_TAG,
                level: 0,
                markup: MD_HEADER_MARKUP,
                lines: [{
                        type: 'inline',
                        tag: '',
                        level: 1,
                        markup: '',
                        content: defaultFirstChapter
                    }]
            })) - 1;
        }
        while (itemIndex < mappedMdContent.mdContentItems.length) {
            const mdContentItem = mappedMdContent.mdContentItems[itemIndex];
            if (mdContentItem.tag == HTML_HEADER_LEVEL_1_TAG && mdContentItem.lines[0].content != '') {
                // Header Level 1 that is not empty starts new Chapter
                chapterIndex = mdChapters.push(new MdChapter(mdContentItem)) - 1;
            }
            else if (mdContentItem.tag != HTML_HEADER_LEVEL_1_TAG) {
                // Process Next Chapter Items with related Footnotes
                addChapterItem(mdChapters[chapterIndex], mdContentItem, mappedMdContent.footNotes);
            }
            itemIndex++;
        }
    }
    writeDebugVarData('mdChapters', mdChapters);
    return mdChapters;
}
exports.mapMdChapters = mapMdChapters;
function addChapterItem(chapter, mdContentItem, footNotes) {
    chapter.addItem(mdContentItem);
    for (const line of mdContentItem.lines) {
        if (line.type == 'inline') {
            // prepare specialized inline content
            let lineContent = line.content;
            while (lineContent.split(FOOTNOTE_MARKUP.start).length > 1) {
                // line(part) contains footnote to append
                const footNoteId = mdlib_ts_1.extractData(lineContent, FOOTNOTE_MARKUP.start, FOOTNOTE_MARKUP.end);
                const footNoteMarkup = `${FOOTNOTE_MARKUP.start}${footNoteId}${FOOTNOTE_MARKUP.end}`;
                const footNote = footNotes.find(footNote => footNote.startsWith(footNoteMarkup));
                if (footNote) {
                    chapter.addFootNote({
                        id: footNoteId,
                        content: footNote.split(footNoteMarkup + ': ')[1]
                    });
                }
                else {
                    console.warn(`Footnote = ${footNoteMarkup} not found!`);
                }
                // split line part that is not yet processed
                lineContent = lineContent.split(footNoteMarkup)[1];
            }
        }
    }
}
/**
 * Map Markdown content strings into typed Markdown Content Items
 * @param mdContentStr Markdown formated string
 * @returns (Array with mapped Markdown Content Items, Array with mapped Footnotes)
 */
function mapMdItems(mdContentStr) {
    // init variables and parse Markdown content data
    const mdContentItems = [];
    const footNotes = [];
    const markdownIt = new markdown_it_1.default();
    const markdownItLines = markdownIt.parse(mdContentStr, {});
    const itemIndex = new ItemIndex(0);
    // map parsed Markdown content data
    while (itemIndex.currentIndex < markdownItLines.length) {
        const itemType = markdownItLines[itemIndex.currentIndex].type.split('_open')[0];
        //console.log(`mapMdItems ==> itemType[${itemIndex.currentIndex}] = ${itemType}`);
        mdContentItems.push(mapMdContentItem(markdownItLines, itemIndex, itemType, footNotes));
        itemIndex.currentIndex++;
    }
    return { mdContentItems, footNotes };
}
/**
 * Map a MarkedIt-parsed object to a typed Markdown Content Item
 * @param markdownItLines Array with parsed MarkedownIt Line-objects (required)
 * @param indexRef Index reference to MarkedownIt Line-object (required)
 * @param itemType Type of Markdown Content Item to map (required)
 * @param footNotes Reference to mapped footnote content (required)
 * @returns Object with typed Markdown Content Item and Lines
 */
function mapMdContentItem(markdownItLines, indexRef, itemType, footNotes) {
    // create initial object item-type and parsed MarkedIt object values
    const contentItem = {
        type: itemType,
        tag: markdownItLines[indexRef.currentIndex].tag,
        level: markdownItLines[indexRef.currentIndex].level,
        markup: markdownItLines[indexRef.currentIndex].markup,
        lines: []
    };
    // create object lines within item-type
    indexRef.currentIndex++;
    while (!lastMarkdownItLineItem(markdownItLines[indexRef.currentIndex], contentItem)) {
        const prepContent = prepareMarkdownItLineContent(markdownItLines[indexRef.currentIndex].content);
        if (markdownItLines[indexRef.currentIndex].content.startsWith(FOOTNOTE_MARKUP.start)) {
            footNotes.push(prepContent);
        }
        else {
            contentItem.lines.push({
                type: markdownItLines[indexRef.currentIndex].type,
                tag: markdownItLines[indexRef.currentIndex].tag,
                level: markdownItLines[indexRef.currentIndex].level,
                markup: markdownItLines[indexRef.currentIndex].markup,
                content: prepContent
            });
        }
        indexRef.currentIndex++;
    }
    return contentItem;
}
/**
 * Determine end of content item lines (on the same level)
 * @param markdownItLine current MarkdownIt line to map
 * @param contentItem current content item to map
 * @returns weither current MarkdownIt line is the end of current content item that is mapped
 */
function lastMarkdownItLineItem(markdownItLine, contentItem) {
    return (markdownItLine.type == contentItem.type + '_close' && markdownItLine.level == contentItem.level);
}
function prepareMarkdownItLineContent(contentInput) {
    let contentOutput = contentInput;
    if (contentInput.split(HTML_IMG_TAG.start).length > 1) {
        // convert HTML image to Markdown variant
        const imageData = mdlib_ts_1.extractData(contentInput, HTML_IMG_TAG.start, HTML_IMG_TAG.end);
        const imageRef = mdlib_ts_1.extractData(imageData, 'src="', '"');
        const imageId = 'imageId' + String(Math.round((Math.random() * 10000)));
        contentOutput = mdlib_ts_1.parseMdImage(imageId, imageRef);
    }
    return contentOutput;
}
/**
 * create Markdown content from Item Object
 * @param mdContentItem Item Object for Markdown Content
 * @returns string with Markdown content
 */
function createMdItemContent(item) {
    let mdContent = '';
    switch (item.type) {
        case 'heading': {
            mdContent = createMdHeaderContent(item);
            break;
        }
        case 'table': {
            mdContent = createMdTableContent(item);
            break;
        }
        case 'bullet_list': {
            mdContent = createMdListContent(item);
            break;
        }
        case 'ordered_list': {
            mdContent = createMdListContent(item);
            break;
        }
        case 'paragraph': {
            mdContent = createMdParagraphContent(item);
            break;
        }
    }
    return mdContent;
}
exports.createMdItemContent = createMdItemContent;
function createMdHeaderContent(item) {
    const headerLevel = Number(item.tag.split('h')[1]);
    const mdMarkup = MD_HEADER_MARKUP.repeat(headerLevel) + ' ';
    return createMdContent(item, mdMarkup);
}
exports.createMdHeaderContent = createMdHeaderContent;
function createMdListContent(item) {
    let mdContent = '';
    let seqNr = 0;
    for (const line of item.lines) {
        switch (line.type) {
            case 'list_item_open': {
                // add list item level spaces and list item ident
                if (line.level > 1) {
                    mdContent += LIST_ITEM_LEVEL_SPACE.repeat(line.level - 1);
                }
                if (item.type == 'ordered_list') {
                    // set specific content for ordered list
                    seqNr++;
                    mdContent += seqNr.toString();
                }
                mdContent += MD_LIST_MARKUP;
                break;
            }
            case 'inline': {
                mdContent += ' ' + line.content + '\n\n';
            }
        }
    }
    return mdContent;
}
exports.createMdListContent = createMdListContent;
function createMdTableContent(item) {
    let mdContent = '';
    let rowContent = '';
    let headContent = '';
    for (const line of item.lines) {
        switch (line.type) {
            case 'th_open': {
                if (!rowContent.trim().endsWith('|')) {
                    rowContent += '| ';
                }
                if (!headContent.trim().endsWith('|')) {
                    headContent += '|';
                }
                headContent += '-';
                break;
            }
            case 'th_close': {
                rowContent += ' | ';
                headContent += '-|';
                break;
            }
            case 'tr_close': {
                mdContent += rowContent.trim() + '\n';
                rowContent = '';
                break;
            }
            case 'thead_close': {
                mdContent += headContent.trim() + '\n';
                headContent = '';
                break;
            }
            case 'inline': {
                rowContent += line.content;
                break;
            }
            case 'td_open': {
                if (!rowContent.trim().endsWith('|')) {
                    rowContent += '| ';
                }
                break;
            }
            case 'td_close': {
                rowContent += ' | ';
                break;
            }
        }
    }
    mdContent += '\n' + rowContent;
    return mdContent;
}
exports.createMdTableContent = createMdTableContent;
function createMdParagraphContent(item) {
    return createMdContent(item);
}
exports.createMdParagraphContent = createMdParagraphContent;
function createMdContent(item, MdMarkup = '') {
    let mdContent = '';
    for (const line of item.lines) {
        mdContent += line.content + '\n';
    }
    return `${MdMarkup}${mdContent}` + '\n';
}
exports.createMdContent = createMdContent;
/**
 * Synchronized execution of PanDoc CLI
 * @param parmsFrom PanDoc -f/--from parms (required)
 * @param parmsTo PanDoc -t/--to parms
 * @param inputFilePath path and name input file
 * @param outputPath path output file's processing (including attached images etc.)
 * @param outputFile main output file name (a Markdown file or PDF, etc.)
 * @returns terminal output execution
 */
function panDocExecSync(parmsFrom, parmsTo, inputFilePath, outputPath, outputFile) {
    // PanDoc CLI execution
    const absInputFilePath = path_1.resolve(inputFilePath);
    md_pre_kroki_1.createNewDirectory(outputPath);
    const panDocArgs = `-f ${parmsFrom.join(' ')} -t ${parmsTo.join(' ')} "${absInputFilePath}" -o "${outputFile}"`;
    console.log(`panDocArgs = ${panDocArgs}`);
    return child_process_1.execSync(`pandoc ${panDocArgs}`, { cwd: outputPath }).toString();
}
exports.panDocExecSync = panDocExecSync;
function writeDebugVarData(varName, varData) {
    md_pre_kroki_1.createNewDirectory(DEBUG_OUTPUT_PATH);
    fs_1.writeFileSync(path_1.resolve(DEBUG_OUTPUT_PATH, `${varName}.json`), JSON.stringify(varData, undefined, '  '));
}
