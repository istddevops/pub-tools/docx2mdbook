/**
 * Doc2Md Utils
 */
/**
 * Converts a google or word docx file to multiple markdown-files (for each section) and the embedded media asset files
 * @param relDocxFilePath relative path and file name docx-document (required)
 * @param mdFilesPath path to directory to write converted Markdown and Media-assets files (required)
 * @param panDocOutputPath temporary PanDoc conversion output path (optional, default = pandoc-output)
 */
export declare function convertDocx2MdBook(docxFilePath: string, mdFilesPath: string, panDocOutputPath?: string): void;
