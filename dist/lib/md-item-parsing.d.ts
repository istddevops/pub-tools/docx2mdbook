/**
 * Markdown Item Parsing functionality
 * Based on MarkdowIt structures
 */
/**
 * Markdown item types that are currently supported by this module
 */
declare type MdItemType = 'heading' | 'paragraph' | 'bullet_list' | 'ordered_list' | 'table' | 'footnote';
/**
 * Markdown line content data
 */
interface MdLine {
    type: string;
    tag: string;
    level: number;
    markup: string;
    content: string;
}
/**
 * Markdown content item data
 */
interface MdContentItem {
    type: MdItemType;
    tag: string;
    level: number;
    markup: string;
    lines: MdLine[];
}
interface MdFootNote {
    id: string;
    content: string;
}
/**
 * Markdown Chapter Content
 */
declare class MdChapter {
    private _chapterItem;
    private _items;
    private _footNotes;
    constructor(_chapterItem: MdContentItem);
    get chapterItem(): MdContentItem;
    /**
     * chapter name
     */
    get chapterName(): string;
    /**
     * chapter name with markup
     */
    get chapterMdName(): string;
    /**
     * Chapter Markdown filename
     */
    get mdFileName(): string;
    /**
     * Chapter Markdown content
     */
    get mdContent(): string;
    /**
     * Add Chapter Markdown content item
     * @param item
     */
    addItem(item: MdContentItem): void;
    /**
     * Chapter Markdown content items
     */
    get items(): MdContentItem[];
    addFootNote(footNote: MdFootNote): void;
    get footNotes(): MdFootNote[];
}
/**
 * Map a Markdown content string into typed Markdown Content Chapters Content Items
 * @param mdContentStr Markdown formated string (required)
 * @param defaultFirstChapter Chapter to be set when there is no starting Header Level 1 (optional)
 * @returns Array Chapters Content Items
 */
export declare function mapMdChapters(mdContentStr: string, defaultFirstChapter?: string): MdChapter[];
/**
 * create Markdown content from Item Object
 * @param mdContentItem Item Object for Markdown Content
 * @returns string with Markdown content
 */
export declare function createMdItemContent(item: MdContentItem): string;
export declare function createMdHeaderContent(item: MdContentItem): string;
export declare function createMdListContent(item: MdContentItem): string;
export declare function createMdTableContent(item: MdContentItem): string;
export declare function createMdParagraphContent(item: MdContentItem): string;
export declare function createMdContent(item: MdContentItem, MdMarkup?: string): string;
/**
 * Synchronized execution of PanDoc CLI
 * @param parmsFrom PanDoc -f/--from parms (required)
 * @param parmsTo PanDoc -t/--to parms
 * @param inputFilePath path and name input file
 * @param outputPath path output file's processing (including attached images etc.)
 * @param outputFile main output file name (a Markdown file or PDF, etc.)
 * @returns terminal output execution
 */
export declare function panDocExecSync(parmsFrom: string[], parmsTo: string[], inputFilePath: string, outputPath: string, outputFile: string): string;
export {};
