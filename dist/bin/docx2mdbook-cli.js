#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Node Package Modules
 */
const process_1 = require("process");
/**
 * CLI Library Modules
 */
const lib_1 = require("../lib");
//
// START CLI Script
//
if (process_1.argv[3] || !process_1.argv[5]) {
    lib_1.convertDocx2MdBook(process_1.argv[2], process_1.argv[3], process_1.argv[4]);
}
else {
    console.error('Excact 2 parms required <DocxFilePath> <MdFilesPath> and 1 optional <panDocOutputPath>');
}
