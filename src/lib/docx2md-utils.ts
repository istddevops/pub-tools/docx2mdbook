/**
 * Doc2Md Utils
 */

/**
 * Node Package Imports
 */
import { readFileSync, existsSync, copyFileSync, writeFileSync } from 'fs';
import { resolve, join } from 'path';
//import { inspect } from 'util';

import { mapMdChapters, panDocExecSync } from './md-item-parsing';
// Open Source van eigen maak (n.t.b. onderbrengen waar?)
import { preProcessKrokiMdContent, writePreProcessedDestFile, createSubDirectories } from '@dgwnu/md-pre-kroki'; 
import { readMdBookConfigFile } from '@pub-tools/mdbook';
import { relativeCopyFile, removeFolder } from '@pub-tools/mdlib-ts';

/**
 * Constant Values
 */
const PANDOC_MD_OUTPUT_FILE = 'converted-by-pandoc.md';
const PANDOC_DOCX_PARMS_FROM = ['docx'];
const PANDOC_MD_PARMS_TO = [
    'markdown_strict+footnotes+pipe_tables', 
    '--wrap=none', '--tab-stop=2', 
    '--extract-media=.'
];
const SUMMARY_MD_FILE = 'SUMMARY.md';
const FIRST_CHAPTER_FILE = 'README.md';
const CONTRIBUTING_FILE = 'CONTRIBUTING.md';
const CHANGELOG_FILE = 'CHANGELOG.md';
const MDBOOK_CONFIG = 'mdbook-config.json';

/**
 * Converts a google or word docx file to multiple markdown-files (for each section) and the embedded media asset files
 * @param relDocxFilePath relative path and file name docx-document (required)
 * @param mdFilesPath path to directory to write converted Markdown and Media-assets files (required)
 * @param panDocOutputPath temporary PanDoc conversion output path (optional, default = pandoc-output)
 */
 export function convertDocx2MdBook(docxFilePath: string, mdFilesPath: string, panDocOutputPath: string = 'pandoc-output') {
    // remove previous created folders
    removeFolder(mdFilesPath);
    removeFolder(panDocOutputPath);

    // create markdown file and extract media from docx file
    docx2MdFile(docxFilePath, panDocOutputPath);

    // split-up converted markdown file into chapters and do some pre-processing (kroki.io, images, footnotes, ...)
    const absPanDocOutputFilePath = resolve(panDocOutputPath, PANDOC_MD_OUTPUT_FILE);
    const mdChapters = mapMdChapters(readFileSync(absPanDocOutputFilePath, 'utf-8'));
    const srcDir = resolve(panDocOutputPath);
    const destDir = resolve(mdFilesPath);
    // use create sub directories for one file to create a sub directories for all output markdown files
    createSubDirectories(resolve('.'), join(mdFilesPath, 'stubfile.md'));
    let firstChapter = true;
    let summaryLines: string[] = [];

    for (const mdChapter of mdChapters) {
        let chapterContent = mdChapter.mdContent;
        let chapterFile = mdChapter.mdFileName;

        if (firstChapter) {
            chapterFile = FIRST_CHAPTER_FILE;
            firstChapter = false;
        }

        console.log(`srcDir = ${srcDir} destDir = ${destDir} chapterFile = ${chapterFile}`);
        writePreProcessedDestFile(srcDir, destDir, join(srcDir, chapterFile), preProcessKrokiMdContent(chapterContent));
        summaryLines.push(createSummaryLine(mdChapter.chapterName, chapterFile));
    }

    // write basic files with default / custom publication content and required configuration
    writeBasicFile(destDir, '# Bijdragen\n\n - Gegenereerd door Docx2MdBook CLI\n', CONTRIBUTING_FILE);
    writeBasicFile(destDir, '# Wijzigingen\n\n - Gegenereerd door Docx2MdBook CLI\n', CHANGELOG_FILE);    
    writeSummaryFile(summaryLines, destDir);
    writeMdBookConfig(destDir);
}

/**
 * Convert a docx file to a  Markdown file (and extract media)
 * @param docxFilePath path / file input docx file
 * @param panDocOutputPath path markdown file (media will be extracted to path)
 */
function docx2MdFile(docxFilePath: string, panDocOutputPath: string) {
    console.log(panDocExecSync(PANDOC_DOCX_PARMS_FROM, PANDOC_MD_PARMS_TO, docxFilePath, panDocOutputPath, PANDOC_MD_OUTPUT_FILE));
}

/**
 * Write a customized basic file found in the repository root or default content if not supplied
 * @param destDir directory path where to write the basic file content (required)
 * @param defaultContent default content in case there is no basic file supllied in root (required)
 * @param fileName basic file name (required)
 */
function writeBasicFile(destDir: string, defaultContent: string, fileName: string) {
    const srcFilePath = resolve('.', fileName);

    if (existsSync(srcFilePath)) {
        // customized basic source file found in root
        const customizedContent = readFileSync(srcFilePath, 'utf-8');
        const srcDir = resolve('.');
        writePreProcessedDestFile(srcDir, destDir, srcFilePath, customizedContent);
    } else {
        // no customized basic source file available
        writeFileSync(resolve(destDir, fileName), defaultContent, 'utf-8');
    }
   
}

/**
 * Create a new summary line from id and linked md file path
 * @param summaryId id or name of summary line (required)
 * @param mdFilePath linked file path (required)
 * @returns a GitBook like formated summary line string
 */
function createSummaryLine(chapterName: string, mdChapterFile: string) {
    return `* [${chapterName}](${mdChapterFile})`;
}

/**
 * write summary lines to destination directory
 * @param summaryLines list with summary lines to write
 * @param destDir destination to write SUMMARY.md file
 */
function writeSummaryFile(summaryLines: string[], destDir: string) {
    const summaryDestFilePath = resolve(destDir, SUMMARY_MD_FILE); 
    const summaryContent = [
        '# Converted from docx to MdBook\n\n',
        '## Over dit document\n\n',
        summaryLines[0] + '\n',
        '* [Bijdragen](' + CONTRIBUTING_FILE + ')\n',
        '* [Wijzigingen](' + CHANGELOG_FILE + ')\n\n',
        '## Inhoud\n\n',
        summaryLines.slice(1).join('\n'),
        '\n'
    ].join('');
    writeFileSync(summaryDestFilePath, summaryContent, 'utf-8')
}

/**
 * Write required MdBook Configuration that should be supplied in the root of the repository
 * @param destDir destination directory where to write the MdBook Configuration
 * 
 * @todo
 * - Generic copy with auto create directory.... (mdlib-ts ??)
 */
function writeMdBookConfig(destDir: string) {

    if (existsSync(MDBOOK_CONFIG)) {
        // copy the config to destination
        const mdBookConfigFilePath = resolve(destDir, MDBOOK_CONFIG);
        copyFileSync(MDBOOK_CONFIG, mdBookConfigFilePath);
        // read tge config and copy linked files (logo etc.)
        const mdBookConfig = readMdBookConfigFile(mdBookConfigFilePath);

        if (mdBookConfig.logo) {
            // copy linked logo file
            relativeCopyFile(mdBookConfig.logo, destDir);
        }

    } else {
        console.warn(`MdBook Configuration file ${MDBOOK_CONFIG} is missing!`)
    }

}