#!/usr/bin/env node
"use strict"

/**
 * Node Package Modules
 */
import { argv } from 'process';

/**
 * CLI Library Modules
 */
import { convertDocx2MdBook } from '../lib'

//
// START CLI Script
//

if (argv[3] || !argv[5]) {
    convertDocx2MdBook(argv[2], argv[3], argv[4]);
} else {
    console.error('Excact 2 parms required <DocxFilePath> <MdFilesPath> and 1 optional <panDocOutputPath>');
}
